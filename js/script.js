let currentColorButton = null;
		function changeColor(key) {
			const buttons = document.querySelectorAll('.btn');
			buttons.forEach(button => {
				if (button.textContent === key) {
					button.classList.add('blue');
					currentColorButton = button;
				} else {
					button.classList.remove('blue');
				}
			});
		}